## 1.0.2（2022-07-15）
1、更新兼容钉钉、支付宝、字节跳动、QQ小程序；
2、增加组件外层customStyle自定义样式属性设置。
## 1.0.1（2022-07-14）
更新兼容系统
## 1.0.0（2022-07-14）
新增虚拟列表组件插件
